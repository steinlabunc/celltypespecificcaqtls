#!/bin/bash
#SBATCH -n 1
#SBATCH -t 24:00:00
#SBATCH --mem 30720
module add ldsc;
#for chr in 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22

#do

python /nas/longleaf/apps/ldsc/1.0.0/ldsc/ldsc.py --l2 --bfile /proj/steinlab/projects/sharedApps/ldsc/1000G_plinkfiles/1000G.mac5eur.$1 --ld-wind-cm 1 --annot /proj/steinlab/projects/R00/ATACPartitionedHeritability/AnnotFiles/CSAW/$1.annot.gz --out /proj/steinlab/projects/R00/ATACPartitionedHeritability/AnnotFiles/CSAW/$1 --print-snps /proj/steinlab/projects/sharedApps/ldsc/hapmap3_snps/hm.$1.snp;


#done

exit 0;
