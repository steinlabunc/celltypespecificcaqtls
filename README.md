**Cell-type specific effects of genetic variation on chromatin accessibility during human neuronal differentiation**

Dan Liang, Angela L. Elwell, Nil Aygün, Michael J. Lafferty, Oleh Krupa, Kerry E. Cheek, Kenan P. Courtney, Marianna Yusupova, Melanie E. Garrett, Allison Ashley-Koch, Gregory E. Crawford, Michael I. Love, Luis de la Torre-Ubieta, Daniel H. Geschwind, Jason L. Stein


If you have any question, feel free to email us.
Dan Liang: danliang@email.unc.edu
Jason Stein: jason_stein@med.unc.edu

