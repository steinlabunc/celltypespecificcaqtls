#!/bin/bash
#SBATCH -p general
#SBATCH -N 1
#SBATCH -n 1
#SBATCH --mem=8g
#SBATCH -t 4:00:00

module add plink/1.90b3;

for chr in `seq 1 22`
do 

plink --bfile $chr.dose.R2g03.QC --freqx --keep KeepIds_Progenitor.txt --out $chr.dose.R2g03.QC.Progenitor;


plink --bfile $chr.dose.R2g03.QC --freqx --keep KeepIds_Neuron.txt --out $chr.dose.R2g03.QC.Neuron;


done

exit 0;


