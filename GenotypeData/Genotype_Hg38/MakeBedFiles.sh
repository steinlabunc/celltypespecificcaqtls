#!/bin/bash

#SBATCH -p general
#SBATCH -N 1
#SBATCH -n 1
#SBATCH --mem=4g
#SBATCH -t 1:00:00
module add ucsctools;
module add plink/1.90b3;
## chr1-22
#chrs=`seq 1 22`;

#for chr in $chrs
#do 

awk '{print "chr"$1,$4,$4+1,$2,$3,$5,$6}' /proj/steinlab/projects/R00/atac-qtl/GenotypeData/PlinkData_original/QCMergedData/Imputation/QCImputationData/QCPlinkableFiles/$1.dose.R2g03.QC.bim > $1.hg19.bed;

liftOver $1.hg19.bed hg19ToHg38.over.chain.gz $1.hg38.bed $1.unlifted.bed;

awk '{print $4}' $1.unlifted.bed > $1.exclude.txt;

plink --bfile /proj/steinlab/projects/R00/atac-qtl/GenotypeData/PlinkData_original/QCMergedData/Imputation/QCImputationData/QCPlinkableFiles/$1.dose.R2g03.QC --exclude $1.exclude.txt --make-bed --out $1.dose.R2g03.QC;

awk '{print "chr"$1,$4,$4+1,$2,$3,$5,$6}' $1.dose.R2g03.QC.bim > $1.hg19.bed;

liftOver $1.hg19.bed hg19ToHg38.over.chain.gz $1.hg38.bed $1.unlifted.bed;

#awk '{print $1,$1":"$2":"$6":"$7,0,$2,$6,$7}' $1.hg38.bed > $1.dose.R2g03.QC.bim;

#awk '{$6=($5==$6? "U":$6)}1' $1.dose.R2g03.QC.bim > $1.dose.R2g03.QC.bim.t;

#mv $1.dose.R2g03.QC.bim.t $1.dose.R2g03.QC.bim;

#done

## chrX
#awk -F'\t' '{print "chrX",$4,$4+1,$2,$3,$5,$6}' /proj/steinlab/projects/R00/atac-qtl/GenotypeData/PlinkData_original/QCMergedData/Imputation/QCImputationData/QCPlinkableFiles/X.PAR.dose.R2g03.QC.bim > X.PAR.hg19.bed;

#liftOver X.PAR.hg19.bed hg19ToHg38.over.chain.gz X.PAR.hg38.bed X.PAR.unlifted.bed;

#awk -F'\t' '{print "X","X:"$2":"$6":"$7,0,$2,$6,$7}' X.PAR.hg38.bed > X.PAR.dose.R2g03.QC.bim;

##chrX

#awk -F'\t' '{print "chrX",$4,$4+1,$2,$3,$5,$6}' /proj/steinlab/projects/R00/atac-qtl/GenotypeData/PlinkData_original/QCMergedData/Imputation/QCImputationData/QCPlinkableFiles/X.Non.PAR.dose.R2g03.QC.bim > X.Non.PAR.hg19.bed;

#liftOver X.Non.PAR.hg19.bed hg19ToHg38.over.chain.gz X.Non.PAR.hg38.bed X.Non.PAR.unlifted.bed;

#awk '{print $4}' X.Non.PAR.unlifted.bed > X.Non.PAR.exclude.txt;

#plink --bfile /proj/steinlab/projects/R00/atac-qtl/GenotypeData/PlinkData_original/QCMergedData/Imputation/QCImputationData/QCPlinkableFiles/X.Non.PAR.dose.R2g03.QC --exclude X.Non.PAR.exclude.txt --make-bed --out X.Non.PAR.dose.R2g03.QC;

#awk -F'\t' '{print "chrX",$4,$4+1,$2,$3,$5,$6}' X.Non.PAR.dose.R2g03.QC.bim > X.Non.PAR.hg19.bed;

#liftOver X.Non.PAR.hg19.bed hg19ToHg38.over.chain.gz X.Non.PAR.hg38.bed X.Non.PAR.unlifted.bed;


#awk -F'\t' '{print "X","X:"$2":"$6":"$7,0,$2,$6,$7}' X.Non.PAR.hg38.bed > X.Non.PAR.dose.R2g03.QC.bim;

#exit 0;













