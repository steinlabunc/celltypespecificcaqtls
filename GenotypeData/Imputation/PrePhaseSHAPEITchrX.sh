#!/bin/bash
#SBATCH -n 16
#SBATCH -t 24:00:00
#SBATCH --mem 30720

##https://genome.sph.umich.edu/wiki/Minimac3_Cookbook_:_Pre-Phasing
## chrX and chrY in plink is chr23 and chr24
## here we impute autosome vcf files first
#chrs=`seq 1 22`;
geneticmapdir='/proj/steinlab/projects/R00/atac-qtl/GenotypeData/PlinkData_original/QCMergedData/Imputation/PrePhasedFiles/1000GP_Phase3';
vcffiledir='/proj/steinlab/projects/R00/atac-qtl/GenotypeData/PlinkData_original/QCMergedData/Imputation/VcfFiles';
#for chr in $chrs; 
#do 
##1) nonPAR
shapeit -check -B $vcffiledir/allmgergedGenoMafHweQC.X.Non.PAR --output-log allmgergedGenoMafHweQC.X.Non.PAR.phased.log --chrX;
shapeit -B $vcffiledir/allmgergedGenoMafHweQC.X.Non.PAR -M $geneticmapdir/genetic_map_chrX_nonPAR_combined_b37.txt -O allmgergedGenoMafHweQC.X.Non.PAR.phased --thread 16 --chrX;
shapeit -convert --input-haps allmgergedGenoMafHweQC.X.Non.PAR.phased --output-vcf $vcffiledir/allmgergedGenoMafHweQC.X.Non.PAR.phased.vcf;
##2) PAR1
shapeit -check -B $vcffiledir/allmgergedGenoMafHweQC.X.PAR1 --output-log allmgergedGenoMafHweQC.X.PAR1.phased.log --chrX;
shapeit -B $vcffiledir/allmgergedGenoMafHweQC.X.PAR1 -M $geneticmapdir/genetic_map_chrX_PAR1_combined_b37.txt -O allmgergedGenoMafHweQC.X.PAR1.phased --thread 16 --chrX;
shapeit -convert --input-haps allmgergedGenoMafHweQC.X.PAR1.phased --output-vcf $vcffiledir/allmgergedGenoMafHweQC.X.PAR1.phased.vcf;
##3)PAR2
shapeit -check -B $vcffiledir/allmgergedGenoMafHweQC.X.PAR2 --output-log allmgergedGenoMafHweQC.X.PAR1.phased.log --chrX;
shapeit -B $vcffiledir/allmgergedGenoMafHweQC.X.PAR2 -M $geneticmapdir/genetic_map_chrX_PAR2_combined_b37.txt -O allmgergedGenoMafHweQC.X.PAR2.phased --thread 16 --chrX;
shapeit -convert --input-haps allmgergedGenoMafHweQC.X.PAR2.phased --output-vcf $vcffiledir/allmgergedGenoMafHweQC.X.PAR2.phased.vcf;
#done

exit 0;










