#!/bin/bash
#SBATCH -n 1
#SBATCH -t 24:00:00
#SBATCH --mem 30720

## chrX and chrY in plink is chr23 and chr24
## here we impute autosome vcf files first
#chrs=`seq 1 22`;
refgenomedir='/proj/steinlab/projects/R00/atac-qtl/GenotypeData/PlinkData_original/QCMergedData/Imputation/Minimac4RefGenome';
vcffiledir='/proj/steinlab/projects/R00/atac-qtl/GenotypeData/PlinkData_original/QCMergedData/Imputation/VcfFiles';
#for chr in $chrs; 
#do 
##1) make vcf files in each chr
minimac4 --refHaps $refgenomedir/$1.1000g.Phase3.v5.With.Parameter.Estimates.m3vcf --haps $vcffiledir/allmgergedGenoMafHweQC.$1.phased.vcf --prefix allmgergedGenoMafHweQC.$1; 
#done

exit 0;










