options(stringsAfFactors=FALSE);
## WASP mapped bam files
bamfiledir="/proj/steinlab/projects/R00/atac-qtl/WASPResult/RemvBlistChrMT/";
## read matched bamfile and DNAID
groupIDs=read.csv("/proj/steinlab/projects/R00/ATACpreprocess/VerifyBamID/VerifyBamIDList.csv");
groupIDs=groupIDs[which(groupIDs$FREEMIX<=0.02),];
for (i in 1:dim(groupIDs)[1]){
    ##input file bam
    input1=paste0(bamfiledir,groupIDs$Names[i],".keep.uniq.NoMTBlist.bam");
    ##input file group ID
    input2=groupIDs$MatchedDNAID[i];
    ##output files
    output=paste0(groupIDs$Names[i],".keep.uniq.NoMTBlist.bam");
    logfile=paste0(groupIDs$Names[i],".out");
    ##submit jobs
    commandline=paste("sbatch -o",logfile,"MakeBams_AddGroups.sh",input1,input2,output);
    system(commandline);
}






