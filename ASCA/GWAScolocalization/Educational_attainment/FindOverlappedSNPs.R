library(GenomicRanges);
library(GenomicFeatures);
library(rtracklayer);
library(GenomicAlignments);
options(stringsAsFactors=FALSE);
##read GWAS data
load("/proj/steinlab/projects/R00/atac-qtl/EMMAXResult/DiseaseColocalization/ConditinalCaQTLs/Educational_attainment/sigGWAS_SNP_coords_hg38.Rdata");
##Allele specific caQTLs
##neuron
NAScaQTLs=read.csv("/proj/steinlab/projects/R00/atac-qtl/AlleleSpeCaQTLs/ASEResults/Neuron_allele_specific_caQTLs_sig.csv");
NAScaSNPGR=GRanges(paste0("chr",sapply(NAScaQTLs$SNP, function(x) unlist(strsplit(x,":",fixed=TRUE))[1])),IRanges(sapply(NAScaQTLs$SNP, function(x) as.numeric(unlist(strsplit(x,":",fixed=TRUE))[2])),sapply(NAScaQTLs$SNP, function(x) as.numeric(unlist(strsplit(x,":",fixed=TRUE))[2]))));
mcols(NAScaSNPGR)=NAScaQTLs;
##progenitor
PAScaQTLs=read.csv("/proj/steinlab/projects/R00/atac-qtl/AlleleSpeCaQTLs/ASEResults/Progenitor_allele_specific_caQTLs_sig.csv");
PAScaSNPGR=GRanges(paste0("chr",sapply(PAScaQTLs$SNP, function(x) unlist(strsplit(x,":",fixed=TRUE))[1])),IRanges(sapply(PAScaQTLs$SNP, function(x) as.numeric(unlist(strsplit(x,":",fixed=TRUE))[2])),sapply(PAScaQTLs$SNP, function(x) as.numeric(unlist(strsplit(x,":",fixed=TRUE))[2]))));
mcols(PAScaSNPGR)=PAScaQTLs;
##overlap
olaps=findOverlaps(NAScaSNPGR, hg38resultGR);
Noverlaps=hg38resultGR[subjectHits(olaps)];
mcols(Noverlaps)=cbind(mcols(Noverlaps),NAScaQTLs[queryHits(olaps),]);
if (length(Noverlaps)>0){
   Noverlaps=unique(Noverlaps);
   cat("#Neuron AScaQTLs",length(Noverlaps),"...\n");
   save(Noverlaps,file="OverlappedSNPs_Neuron.Rdata");
}

olaps=findOverlaps(PAScaSNPGR, hg38resultGR);
Poverlaps=hg38resultGR[subjectHits(olaps)];
mcols(Poverlaps)=cbind(mcols(Poverlaps),PAScaQTLs[queryHits(olaps),]);
if (length(Poverlaps)>0){
   Poverlaps=unique(Poverlaps);
   cat("#Progenitor AScaQTLs",length(Poverlaps),"...\n");
   save(Poverlaps,file="OverlappedSNPs_Progenitor.Rdata");
}





