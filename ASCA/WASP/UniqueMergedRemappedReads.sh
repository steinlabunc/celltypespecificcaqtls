#!/bin/bash

#SBATCH -p general
#SBATCH -N 1
#SBATCH -n 1
#SBATCH --mem=15g
#SBATCH -t 4:00:00

module add wasp;
module add samtools;

python3 /nas/longleaf/apps/wasp/2018-07/WASP/mapping/rmdup_pe.py $1 $2;

samtools sort -o $3 $2;

samtools index $3;

rm $2;
exit 0;






