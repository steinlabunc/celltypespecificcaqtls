## This script is to use BWA men map remapped data to human genome (hg38)
##module add bwa/0.7.15
##module add r/3.4.1
options(stringsAsFactors=FALSE);
## set dir for raw data
Keepremapfile = "/pine/scr/d/a/danliang/WASPResult/RemappedFiles";
##remapped dir
Remappedfile="/proj/steinlab/projects/R00/atac-qtl/WASPResult/FilterRemappedReads/";
outputdir="/proj/steinlab/projects/R00/atac-qtl/WASPResult/MergedFiles/";
#find files list in raw data directory 
Keepremapbam <- list.files(Keepremapfile,full.names=T,recursive=T,pattern=glob2rx("*_adaptertrimed_sort.keep.bam"));

#For loop to provide both paired end reads with each iteration of i
for (i in 1:length(Keepremapbam)) {
#both inputs will be looped through with each itiration of i found in the directory
input1 = Keepremapbam[i];
id = unlist(strsplit(Keepremapbam[i],"/"))[length(unlist(strsplit(Keepremapbam[i],"/")))];
id = unlist(strsplit(id,"_"))[1];
input2 = paste0(Remappedfile,id,"_remap.keep.bam");
name = paste0(id,".keep.merge.bam");  
output1 = paste0(outputdir,name);
output2 = paste0(outputdir,id,".keep.merge.sort.bam");
outfile = paste0(outputdir,id,".out");
##Run BAW men
system(paste("sbatch -o",outfile,"./MergedRemappedReads.sh",input1,input2,output1,output2));
}




