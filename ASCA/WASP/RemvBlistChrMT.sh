#!/bin/bash
## moudle add samtools/1.4
## moudle add picard/2.2.4
## moudle add java/1.8.0_112

#SBATCH -n 1
#SBATCH -t 24:00:00
#SBATCH --mem 30720

module add samtools;
module add bedtools;

samtools view -bh $1 1  2  3  4  5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 X Y -o $2;

samtools index $2;

bedtools intersect -v -abam $2 -b wgEncodeHg38ConsensusSignalArtifactRegions.bed > $3;

samtools index $3;

rm $2;
rm $2.bai;

exit 0;
