#!/bin/bash
#SBATCH --ntasks=1
#SBATCH --time=24:00:00
#SBATCH --mem=50g

module add wasp;
#/nas/longleaf/apps/wasp/2018-07/WASP/snp2h5/snp2h5 --chrom hg38.chromInfo.txt.gz --format vcf --haplotype haplotypes.h5 --snp_index snp_index.h5 --snp_tab snp_tab.h5 /proj/steinlab/projects/R00/atac-qtl/WASPResult/GenotypeHG38CHR/*.dose.R2g03.hg38.vcf.gz;

/nas/longleaf/apps/wasp/2018-07/WASP/snp2h5/snp2h5 --chrom hg38.chromInfo.txt.gz --format vcf --haplotype haplotypes.h5 --snp_index snp_index.h5 --snp_tab snp_tab.h5 /proj/steinlab/projects/R00/atac-qtl/AlleleSpeCaQTLs/VCFWithOnlyBiallelicSNPs/*.dose.R2g03.hg38.bial.newcogtin.sort.vcf.gz;
exit 0;

