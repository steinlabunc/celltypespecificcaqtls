#!/bin/bash
## This script is to remove duplicate reads(picard) and chrM reads(samtools) from bam files
## And also use samtools to index all output files
## Then use picard to get insert size matrix
## moudle add samtools/1.4
## moudle add picard/2.2.4
## moudle add java/1.8.0_112

#SBATCH -n 1
#SBATCH -t 24:00:00
#SBATCH --mem 30720


java -jar /nas/longleaf/apps/picard/2.10.3/picard-2.10.3/picard.jar MarkDuplicates I=$1 O=$2 M=$3 REMOVE_DUPLICATES=TRUE;

samtools index $2;

samtools view -bh $2 chr1 chr2 chr3 chr4 chr5 chr6 chr7 chr8 chr9 chr10 chr11 chr12 chr13 chr14 chr15 chr16 chr17 chr18 chr19 chr20 chr21 chr22 chrX chrY -o $4;

samtools index $4;

java -jar /nas/longleaf/apps/picard/2.10.3/picard-2.10.3/picard.jar CollectInsertSizeMetrics ASSUME_SORTED=TRUE M=0.05 I=$4 O=$5 H=$6;

exit 0;
