#!/bin/bash
#SBATCH -n 1
#SBATCH -t 24:00:00
#SBATCH --mem 10240

bbduk.sh -Xmx1g in1=$1 in2=$2 out1=$3 out2=$4 ref=$5 ktrim=r k=23 hdist=1 minlength=30 mink=11 tpe tbo;
