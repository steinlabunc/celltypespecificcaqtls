##This script is to sort and index sam files
##module add samtools/1.4
#!/bin/bash
samtools sort -o $2 $1;
samtools index $2;

exit 0;

