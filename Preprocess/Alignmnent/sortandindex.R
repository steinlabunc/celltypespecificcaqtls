## This file is to use samtools sort and index the sam files
## sam files are mapped files from fastq files after adapter trimming (BWA mem)
## module add r/3.2.2
## module add samtools/1.4

options(stringsAsFactors=FALSE);
## set dir for sam data
samfile = "/proj/steinlab/projects/R00/ATACpreprocess/BWAmenMap/20170703/";
## Output directory of sorted and indexed files
outputdir = "/proj/steinlab/projects/R00/ATACpreprocess/BWAmenMap/20170703/";
#find files list in sam data directory 
fileslist <- list.files(samfile,pattern="*.sam");
for (i in 1:length(fileslist)) {
    id = unlist(strsplit(fileslist[i],".",fixed=TRUE))[1]
    name = paste0(id,"_sort.bam");
    output = paste0(outputdir,name);
    input = paste0(samfile,fileslist[i]);
    system(paste("bsub -q day -M 10 -n 1 -o sortandindex.log ./sortandindex.sh",input,output));
}
