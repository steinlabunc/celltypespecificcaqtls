#!/bin/bash
## This is to remove the regions in blacklist (bedtools)
## module add samtools/1.5
## module add java/1.8.0_131
## module add macs/2016-02-15
## module add bedtools/2.26    

#SBATCH -n 1
#SBATCH -t 24:00:00
#SBATCH --mem 30720

## index the bam files
echo "Removing Blacklist Regions...";
bedtools intersect -v -abam $1 -b /proj/steinlab/projects/atacseq/MinimalNecessary_Dan/BlackListhg19/wgEncodeHg38ConsensusSignalArtifactRegions.bed > $2;

echo "Indexing...";
samtools view -h -o $7 $2;
samtools view -bS -o $2 $7;
samtools index $2;

echo 'Peak calling...';
macs2 callpeak -t $2 -f BAM -g hs -n $3 -q 0.05 --nolambda --nomodel --outdir $6;

echo "Ploting Insert Size Mtrics...";
java -jar /nas/longleaf/apps/picard/2.10.3/picard-2.10.3/picard.jar CollectInsertSizeMetrics ASSUME_SORTED=TRUE M=0.05 I=$2 O=$4 H=$5;

rm $7;

exit 0;
