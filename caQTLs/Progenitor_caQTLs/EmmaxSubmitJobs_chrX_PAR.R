options(stringAsFactors=FALSE);
# ATAC-seq peak
peaks=read.csv("/proj/steinlab/projects/R00/ATACpreprocess/DifferentChromAccess/CSAW/Invitro/DESeq2/Csaw_DESeq2_result_in_vitro_hg38_1L.csv",row.names=1);
peaks=peaks[which(peaks$seqnames=="chrX"),]
## files dirs
Genofile = '/proj/steinlab/projects/R00/atac-qtl/EMMAXResult/Genotype_Hg38/TruncatedFiles/Progenitor/chrX/X.PAR.dose.R2g03.QC';
PPhenfile = '/proj/steinlab/projects/R00/atac-qtl/EMMAXResult/Phonetype_ATAC/CSAW/Progenitor/';
KinM = '/proj/steinlab/projects/R00/atac-qtl/EMMAXResult/KinshipMatrix/';
##covariants
Pcova="/proj/steinlab/projects/R00/atac-qtl/EMMAXResult/Phonetype_ATAC/CSAW/PCAs/MDSs10.Progenitor.PCA1_4.sex.GW.emmax";
##output dirs
Poutdir = '/proj/steinlab/projects/R00/atac-qtl/EMMAXResult/Progenitor/CSAW/PCAs_4/Progenitor/RawData/chrX_PAR/';
#for (i in 15:length(Pchrs)){
    ##out put file dir
    thisPoutdir=Poutdir;
    ##Genotype dir
    thisPGenofiles=Genofile;
    ## kinship matrix
    PKinMlist=paste0(KinM,"Progenitor.X.BN.kinf");
        numfile=floor(dim(peaks)[1]/1000)+1;
        for (m in 1:numfile){
            cat(c("#!/bin/bash","#SBATCH -n 1","#SBATCH --mem=10g","#SBATCH -t 24:00:00","module add emmax;"),file=paste0("EmmaxCSAW.chrX.PAR.",m,".sh"),sep="\n");
            for (j in (1000*(m-1)+1):min(dim(peaks)[1],(1000*m))){
                name=paste(peaks$seqnames[j],peaks$start[j],peaks$end[j],sep="_");
                thisPPhenofile = paste0(PPhenfile,name,"_Progenitor.txt");
                Poutput = paste0(thisPoutdir,name,"_Progenitor");
                Pcomdline=paste0("emmax -v -d 10 -t ",thisPGenofiles," -p ",thisPPhenofile," -k ",PKinMlist," -c ",Pcova," -o ",Poutput,";");
                cat(Pcomdline,file=paste0("EmmaxCSAW.chrX.PAR.",m,".sh"),append=TRUE,sep="\n");
           }
           cat("exit 0;",file=paste0("EmmaxCSAW.chrX.PAR.",m,".sh"),append=TRUE,sep="\n");
           cat("EmmaxCSAW chrX PAR",m,"...\n");
           system(paste0("sbatch -o ",paste0("EmmaxCSAW.chrX.PAR.",m,".out "),paste0("EmmaxCSAW.chrX.PAR.",m,".sh")));
         }



