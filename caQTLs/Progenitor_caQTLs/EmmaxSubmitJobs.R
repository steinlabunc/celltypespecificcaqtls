options(stringAsFactors=FALSE);
args <- commandArgs(trailingOnly = TRUE);

##chrs
Gchrs=c(1:22);
Pchrs=paste0("chr",c(1:22));
## files dirs
Genofile = '/proj/steinlab/projects/R00/atac-qtl/EMMAXResult/Genotype_Hg38/TruncatedFiles/';
NPhenfile = '/proj/steinlab/projects/R00/atac-qtl/EMMAXResult/Phonetype_ATAC/CSAW/Neuron/';
PPhenfile = '/proj/steinlab/projects/R00/atac-qtl/EMMAXResult/Phonetype_ATAC/CSAW/Progenitor/';
KinM = '/proj/steinlab/projects/R00/atac-qtl/EMMAXResult/KinshipMatrix/';
##covariants
Ncova="/proj/steinlab/projects/R00/atac-qtl/EMMAXResult/Phonetype_ATAC/CSAW/PCAs/MDSs10.Neuron.PCA1_4.sex.GW.emmax";
Pcova="/proj/steinlab/projects/R00/atac-qtl/EMMAXResult/Phonetype_ATAC/CSAW/PCAs/MDSs10.Progenitor.PCA1_4.sex.GW.emmax";
##output dirs
Noutdir = '/pine/scr/d/a/danliang/EMMAXResult/CSAW/PCAs_4/Neuron/';
Poutdir = '/pine/scr/d/a/danliang/EMMAXResult/CSAW/PCAs_4/Progenitor/';
#for (i in 15:length(Pchrs)){
    i=as.numeric(args[2]);
    ##out put file dir
    thisNoutdir=paste0(Noutdir,Pchrs[i],"/");
    thisPoutdir=paste0(Poutdir,Pchrs[i],"/");
    system(paste0("mkdir ",thisNoutdir));
    system(paste0("mkdir ",thisPoutdir));
    ##Genotype dir
    thisNGenodir=paste0(Genofile,"Neuron/",Pchrs[i]);
    thisNGenofiles=list.files(path=thisNGenodir,full.names=T,pattern=glob2rx("*.map"));
    thisPGenodir=paste0(Genofile,"Progenitor/",Pchrs[i]);
    thisPGenofiles=list.files(path=thisPGenodir,full.names=T,pattern=glob2rx("*.map"));
    ## kinship matrix
    NKinMlist=paste0(KinM,"Neuron.",Gchrs[i],".BN.kinf");
    PKinMlist=paste0(KinM,"Progenitor.",Gchrs[i],".BN.kinf");
    if (length(thisNGenofiles)==length(thisPGenofiles)){
        numfile=floor(length(thisNGenofiles)/1000)+1;
        for (m in 1:numfile){
            cat(c("#!/bin/bash","#SBATCH -n 1","#SBATCH --mem=10g","#SBATCH -t 24:00:00","module add emmax;"),file=paste0("EmmaxCSAW",".chr",i,".",m,".sh"),sep="\n");
            for (j in (1000*(m-1)+1):min(length(thisNGenofiles),(1000*m))){
                thisNGenofile=unlist(strsplit(thisNGenofiles[j],".",fixed=T))[1];
                thisPGenofile=unlist(strsplit(thisPGenofiles[j],".",fixed=T))[1];                
                name=unlist(strsplit(thisNGenofile,"/",fixed=T))[length(unlist(strsplit(thisNGenofile,"/",fixed=T)))];
                thisNPhenofile = paste0(NPhenfile,name,"_Neuron.txt");
                thisPPhenofile = paste0(PPhenfile,name,"_Progenitor.txt");
                if(file.exists(thisNPhenofile)&file.exists(thisPPhenofile)){
                  Noutput = paste0(thisNoutdir,name,"_Neuron");
                  Poutput = paste0(thisPoutdir,name,"_Progenitor");
                  Ncomdline=paste0("emmax -v -d 10 -t ",thisNGenofile," -p ",thisNPhenofile," -k ",NKinMlist," -c ",Ncova," -o ",Noutput,";");
                  cat(Ncomdline,file=paste0("EmmaxCSAW",".chr",i,".",m,".sh"),append=TRUE,sep="\n");
                  Pcomdline=paste0("emmax -v -d 10 -t ",thisPGenofile," -p ",thisPPhenofile," -k ",PKinMlist," -c ",Pcova," -o ",Poutput,";");
                  cat(Pcomdline,file=paste0("EmmaxCSAW",".chr",i,".",m,".sh"),append=TRUE,sep="\n");
                }else{
                cat(thisNPhenofile,thisPPhenofile,"does NOT exists!!...\n");
                }
           }
           cat("EmmaxCSAW","chr",i,m,"...\n");
           system(paste0("sbatch -o ",paste0("EmmaxCSAW",".chr",i,".",m,".out "),paste0("EmmaxCSAW",".chr",i,".",m,".sh")));
     }
   }else{cat("Genotype files are not equal for Neuron and Progenitor on chromosome ",i,"...\n");}
#}



