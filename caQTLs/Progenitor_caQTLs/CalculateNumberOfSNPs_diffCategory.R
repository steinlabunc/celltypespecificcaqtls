## used index SNPs and their LD Buddies (r^2 = 1 or 0.5<= r^2 <1)
library(DESeq2);
library(GenomicRanges);
options(stringsAsFactors=FALSE);
## include index SNPs LD buddies (0.5 <= r^2 < =1)
ClumpedPcaQTLs_wLD=read.csv("Progenitor_caQTLs_clumped_wLD_SNPs_PC1_4.csv",row.names=1);
ClumpedPcaQTLs_wLD$B_BP=sapply(ClumpedPcaQTLs_wLD$SNP_B, function(x) unlist(strsplit(x,":"))[2]);
ClumpedPcaQTLs_wLD$B_InPeak=(ClumpedPcaQTLs_wLD$B_BP >= ClumpedPcaQTLs_wLD$peakstart & ClumpedPcaQTLs_wLD$B_BP <= ClumpedPcaQTLs_wLD$peakend);
ClumpedPcaQTLs_wLD$SNPPeakIDs=paste(ClumpedPcaQTLs_wLD$SNP,ClumpedPcaQTLs_wLD$PeakID,sep="_");
caPeakGR=GRanges(ClumpedPcaQTLs_wLD$seqnames,IRanges(ClumpedPcaQTLs_wLD$peakstart,ClumpedPcaQTLs_wLD$peakend));
mcols(caPeakGR)=ClumpedPcaQTLs_wLD$PeakID;
caPeakGR=unique(caPeakGR);
## #Index caSNP and #Index caSNP with SNPs 0.5<= r^2 <=1 in its caPeak
C1=unique(paste0(ClumpedPcaQTLs_wLD$SNP,"_",ClumpedPcaQTLs_wLD$PeakID)[which(ClumpedPcaQTLs_wLD$B_InPeak)]);
cat("Index caSNP and #SNPs with 0.5<= r^2 <=1 of index SNP in its caPeak",length(C1),"...\n");
#make the LD SNPs a GRange
ClumpedPcaSNPs_wLD=GRanges(ClumpedPcaQTLs_wLD$seqnames,IRanges(as.numeric(ClumpedPcaQTLs_wLD$B_BP),as.numeric(ClumpedPcaQTLs_wLD$B_BP)));
ClumpedPcaPeaks_wLD=GRanges(ClumpedPcaQTLs_wLD$seqnames,IRanges(ClumpedPcaQTLs_wLD$peakstart,ClumpedPcaQTLs_wLD$peakend));
##LD budies overlapping caPeaks
polap=findOverlaps(ClumpedPcaSNPs_wLD,ClumpedPcaPeaks_wLD);
index=which(queryHits(polap)!=subjectHits(polap));
C2=unique(paste(ClumpedPcaQTLs_wLD$SNP[queryHits(polap)],ClumpedPcaQTLs_wLD$PeakID[queryHits(polap)],sep="_")[index]);
C2=setdiff(C2,intersect(C2,C1));
#cat("#SNPs with Index caSNP 0.5<= r^2 <=1 in other caPeak",length(C2),"...\n");
##aimed SNPs
cor_ClumpedPcaQTLs_wLD=ClumpedPcaQTLs_wLD[which(ClumpedPcaQTLs_wLD$SNPPeakIDs %in% C2),];
cor_ClumpedPcaQTLs_wLD=cor_ClumpedPcaQTLs_wLD[which(cor_ClumpedPcaQTLs_wLD$B_InPeak=="FALSE"),];
B_SNPGR=GRanges(cor_ClumpedPcaQTLs_wLD$seqnames,IRanges(as.numeric(cor_ClumpedPcaQTLs_wLD$B_BP),as.numeric(cor_ClumpedPcaQTLs_wLD$B_BP)));
##overlap
olaps=findOverlaps(B_SNPGR,caPeakGR);
olapcor_ClumpedPcaQTLs_wLD=cor_ClumpedPcaQTLs_wLD[queryHits(olaps),];
olapcor_ClumpedPcaQTLs_wLD$cor_Peak=paste(seqnames(caPeakGR),start(caPeakGR),end(caPeakGR),sep="_")[subjectHits(olaps)];
##Peaks
corPeaks=unique(data.frame(SNP=olapcor_ClumpedPcaQTLs_wLD$SNP,leadPeak=olapcor_ClumpedPcaQTLs_wLD$PeakID,corPeak=olapcor_ClumpedPcaQTLs_wLD$cor_Peak));
##batch corrected peak counts
load("/proj/steinlab/projects/R00/atac-qtl/EMMAXResult/Phonetype_ATAC/CSAW/Limma_RemvBatch_CSAWcounts.Rdata");
correctedcounts=assay(vsd);
##load meta data
fnamesamplesheet = "/proj/steinlab/projects/R00/ATACpreprocess/DifferentChromAccess/ATACsampleinfo_noblacklist_CC4toCC20_remvdup.csv";
samplesheet = read.csv(fnamesamplesheet,header=TRUE);
colnames(correctedcounts)=samplesheet$SampleID;
## load peak coodinates
peaks=read.csv("/proj/steinlab/projects/R00/ATACpreprocess/DifferentChromAccess/CSAW/Invitro/Limma/Csaw_Limma_result_in_vitro_hg38_1L.csv");
peaks$seqnames=sapply(peaks$seqnames, function(x) substr(x,4,nchar(as.character(x))));
peakID=paste(peaks$seqnames,peaks$start,peaks$end,sep="_");
## use corrected bamID
verifiedIDs=read.csv("/proj/steinlab/projects/R00/ATACpreprocess/VerifyBamID/VerifyBamIDList.csv");
verifiedIDs=verifiedIDs[which(verifiedIDs$FREEMIX<=0.02),];
## keep phenotype files as the same order as genotype files
Pfam=read.table("/proj/steinlab/projects/R00/atac-qtl/EMMAXResult/Genotype_Hg38/TruncatedFiles/Progenitor/chr22/chr22_50807891_50807960.tfam");
PIDs=paste0(Pfam$V1,"_",Pfam$V2);
## Progenitor
PverifiedIDs=verifiedIDs[which(verifiedIDs$CellType=="Progenitor"),];
correctedPcounts=correctedcounts[,match(PverifiedIDs$Names,colnames(correctedcounts))];
Pcoverage=correctedPcounts[,match(PIDs,PverifiedIDs$MatchedDNAID)];
rownames(Pcoverage)=peakID;
corPeaks$cor=NA;
corPeaks$P=NA;
for (i in 1:dim(corPeaks)[1]){
    testGR=GRanges(unlist(strsplit(corPeaks$leadPeak[i],"_"))[1],IRanges(as.numeric(unlist(strsplit(corPeaks$leadPeak[i],"_"))[2])-2000000,as.numeric(unlist(strsplit(corPeaks$leadPeak[i],"_"))[3])+2000000));
    testcaPeakGR=caPeakGR[subjectHits(findOverlaps(testGR,caPeakGR))];
    testdata=data.frame(leadPeak=corPeaks$leadPeak[i],corPeak= testcaPeakGR$X[which(testcaPeakGR$X != corPeaks$leadPeak[i])],cor=NA,P=NA);
    for (j in 1:dim(testdata)[1]){
        leadpeakcounts=Pcoverage[which(rownames(Pcoverage)==testdata$leadPeak[j]),];
        corpeakcounts=Pcoverage[which(rownames(Pcoverage)==testdata$corPeak[j]),];
        thistest=cor.test(leadpeakcounts,corpeakcounts);
        testdata$cor[j]=thistest$estimate;
        testdata$P[j]=thistest$p.value;
    }
    testdata$FDR=p.adjust(testdata$P,method="fdr");
    ind=which(testdata$corPeak==corPeaks$corPeak[i]);
    corPeaks$cor[i]=testdata$cor[ind];
    corPeaks$P[i]=testdata$P[ind];
    corPeaks$FDR[i]=testdata$FDR[ind];
}
save(corPeaks,file="PeaksCor.Rdata");
C2=unique(paste(corPeaks$SNP,corPeaks$leadPeak,sep="_")[which(corPeaks$FDR<0.05)]);
cat("#Index SNPs+#SNPs with Index caSNP 0.5<=r^2<=1 in correlated other caPeaks",length(C2),"...\n");
##make GRanges for open peaks in Neuron
diffacce=peaks;
diffacceGR=GRanges(diffacce$seqnames,IRanges(as.numeric(diffacce$start),as.numeric(diffacce$end)));
mcols(diffacceGR)=diffacce;
diffacceGR=diffacceGR[which(diffacceGR$logFC>0)];
olap=findOverlaps(caPeakGR,diffacceGR);
diffacceGR=diffacceGR[-subjectHits(olap)];
olap=findOverlaps(ClumpedPcaSNPs_wLD,diffacceGR);
C3=unique(paste(ClumpedPcaQTLs_wLD$SNP,ClumpedPcaQTLs_wLD$PeakID,sep="_")[queryHits(olap)]);
C3=setdiff(C3,intersect(C3,c(C1,C2)));
cat("#Index SNPs+#SNPs with Index caSNP 0.5<=r^2<=1 in other Progenitor open Peak",length(C3),"...\n");
##ChromHMM states
ChromHMMfiles = "/proj/steinlab/projects/R00/ChromHMM/E081_25_imputed12marks_dense_hg38.bed";
ChromHMMfile = read.table(ChromHMMfiles);
colnames(ChromHMMfile)=c("chrom","chromStart","chromEnd","name","score","strand","thickStart","thickEnd","itemRgb");
thisstateGR = GRanges(ChromHMMfile$chrom,IRanges(ChromHMMfile$chromStart,ChromHMMfile$chromEnd));
mcols(thisstateGR)=ChromHMMfile;
##ADD SNPpeakID
ClumpedPcaQTLs_wLD$SNPPeakID=paste(ClumpedPcaQTLs_wLD$SNP,ClumpedPcaQTLs_wLD$PeakID,sep="_");
SNPPeakIDs=c(C1,C2,C3);
ClumpedPcaQTLs_wLD=ClumpedPcaQTLs_wLD[-which(ClumpedPcaQTLs_wLD$SNPPeakID %in% SNPPeakIDs),];
ClumpedPSNP_wLD=GRanges(paste0("chr",ClumpedPcaQTLs_wLD$seqnames),IRanges(as.numeric(ClumpedPcaQTLs_wLD$B_BP),as.numeric(ClumpedPcaQTLs_wLD$B_BP)));
mcols(ClumpedPSNP_wLD)=ClumpedPcaQTLs_wLD$SNPPeakID;
ClumpedPSNP_wLD$States="Unknown";
olaps=findOverlaps(ClumpedPSNP_wLD,thisstateGR);
ClumpedPSNP_wLD$States[queryHits(olaps)]=thisstateGR$name[subjectHits(olaps)];
active=c("1_TssA","2_PromU","3_PromD1","4_PromD2","22_PromP","23_PromBiv","13_EnhA1","14_EnhA2","15_EnhAF","16_EnhW1","17_EnhW2","18_EnhAc","5_Tx5'","6_Tx","7_Tx3'","8_TxWk","9_TxReg","10_TxEnh5'","11_TxEnh3'","12_TxEnhW","19_DNase");
C4=unique(ClumpedPSNP_wLD[which(ClumpedPSNP_wLD$States %in% active)]$X);
cat("#Index SNPs+#SNPs with Index caSNP 0.5<=r^2<=1 in Active states",length(C4),"...\n");
save(C1,C2,C3,C4,file="CalculateNumberOfSNPs_diffCategory.Rdata");







