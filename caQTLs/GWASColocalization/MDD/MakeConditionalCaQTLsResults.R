##module add r/3.6.0
options(stringsAsFactors=FALSE);
##load significant GWAS file
load("sigGWAS_SNP_coords_hg38.Rdata");
##Neuron Overlapped SNPs
load("OverlappedSNPs_Neuron.Rdata");
output=unique(output);
##get peak ID
NpeakIDs=paste(output$chrnames,output$peakstart,output$peakend,sep="_");
uNpeakIDs=unique(NpeakIDs);
##covariants
vars=read.table("/proj/steinlab/projects/R00/atac-qtl/EMMAXResult/Phonetype_ATAC/CSAW/PCAs/MDSs10.Neuron.PCA1_7.sex.GW.emmax");
##Ndir for genotype file
Ndir="/proj/steinlab/projects/R00/atac-qtl/EMMAXResult/Genotype_Hg38/TruncatedFiles/Neuron/";
##save dat for plot
plotdatr=GRanges();
#for (j in 1:length(uNpeakIDs)){
#    ##original SNP
#    NSNPs=read.table(paste0(Ndir,"chr",unlist(strsplit(uNpeakIDs[j],"_"))[1],"/","chr",uNpeakIDs[j],".tped"),header=FALSE);
#    NSNPsGR=GRanges(paste0("chr",NSNPs$V1),IRanges(sapply(NSNPs$V2, function(x) as.numeric(unlist(strsplit(x,":",fixed=T))[2])),sapply(NSNPs$V2, function(x) as.numeric(unlist(strsplit(x,":",fixed=T))[2]))));
#    NSNPsGR$SNP_ID=NSNPs$V2;
#    ##Neuron donor IDs
#    NIDfile=read.table(paste0(Ndir,"chr",unlist(strsplit(uNpeakIDs[j],"_"))[1],"/","chr",uNpeakIDs[j],".tfam"),header=FALSE);
#    NIDs=paste(NIDfile$V1,NIDfile$V2,sep="_");
#    ## use one peaks each loop
#    thisoutput=output[which(NpeakIDs==uNpeakIDs[j])];
#    ## genotype
#    geno=paste0(Ndir,"chr",unlist(strsplit(uNpeakIDs[j],"_"))[1],"/","chr",uNpeakIDs[j]);
#    ## phenotype
#    pheno=paste0("/proj/steinlab/projects/R00/atac-qtl/EMMAXResult/Phonetype_ATAC/CSAW/Neuron/chr",uNpeakIDs[j],"_Neuron.txt");
#    ## kinship matrix
#    kinship=paste0("/proj/steinlab/projects/R00/atac-qtl/EMMAXResult/KinshipMatrix/Neuron.",unique(thisoutput$chrnames[1]),".BN.kinf");
#    ## genotype on this chromosome
#    traw=read.table(paste0("/proj/steinlab/projects/R00/atac-qtl/EMMAXResult/Genotype_Hg38/PlinkPedMapFiles/",unique(thisoutput$chrnames[1]),".dose.R2g03.QC.traw"),header=TRUE);
#    ##find GWAS index SNPs
#    ##overlapped SNPs
#    olap=findOverlaps(NSNPsGR,hg38resultGR);
#    ohg38resultGR=hg38resultGR[subjectHits(olap)];
#    ohg38resultGR$SNP_ID=NSNPsGR$SNP_ID[queryHits(olap)];
#    ##calculate LD r2,only keep the SNPs have rsid
#    thisoutput=thisoutput[grep("rs",thisoutput$MarkerName)];
#    for (i in 1:length(thisoutput)){
#        ##find index gSNP 
#        testSNP=thisoutput$MarkerName[i];
#        system(paste0("module add plink/1.90b3;plink --bfile /proj/steinlab/projects/1000genomes/phase3EURhg38/ALL.chr",thisoutput$CHR_A[1],"_GRCh38.genotypes.20170504.EUR --r2 --ld-window-r2 0.2 --ld-window-kb 10000 --ld-window 2000 --ld-snp ",testSNP));
#        LD=read.table("plink.ld",header=T);
#        SNP_BGR=GRanges(paste0("chr",c(unique(LD$CHR_A),LD$CHR_B)),IRanges(c(unique(LD$BP_A),LD$BP_B),c(unique(LD$BP_A),LD$BP_B)));
#        thisohg38resultGR=ohg38resultGR[unique(queryHits(findOverlaps(ohg38resultGR,SNP_BGR)))];
#        ## SNP ID with the min Pvalue
#        thisSNP=thisohg38resultGR$SNP_ID[which(thisohg38resultGR$P < 5e-08)];
#        thisrsid=thisohg38resultGR$MarkerName[which(thisohg38resultGR$P < 5e-08)];
#        for (m in 1:length(thisrsid)){
#            thisoutput$condSNP=thisrsid[m];
#            ## genotype for this SNP
#            thistraw=data.frame(geno=traw[which(traw$SNP==thisSNP[m]),match(NIDs,colnames(traw))]);
#            ## add genotype to co-variants file 
#            thisvars=cbind(vars,t(thistraw));
#            write.table(thisvars,file="thisvars.txt",quote=F,row.names=F,col.names=F);
#            ## run emmax
#            commandline=paste0("module add emmax;emmax -v -d 10 -t ",geno," -p ",pheno, " -k ",kinship," -c thisvars.txt -o chr",uNpeakIDs[j],"_",thisrsid[m],"_Neuron");
#            system(commandline);
#        }
#    }
#    plotdatr=c(plotdatr,thisoutput);
#}
#save(plotdatr,file="Plotdata_Neuron.Rdata");

## Progenitor Overlapped SNPs
load("OverlappedSNPs_Progenitor.Rdata");
output=unique(output);
##get peak ID
PpeakIDs=paste(output$chrnames,output$peakstart,output$peakend,sep="_");
uPpeakIDs=unique(PpeakIDs);
##covariants
vars=read.table("/proj/steinlab/projects/R00/atac-qtl/EMMAXResult/Phonetype_ATAC/CSAW/PCAs/MDSs10.Progenitor.PCA1_4.sex.GW.emmax");
##Ndir for genotype file
Pdir="/proj/steinlab/projects/R00/atac-qtl/EMMAXResult/Genotype_Hg38/TruncatedFiles/Progenitor/";
##save dat for plot
plotdatr=GRanges();
for (j in 1:length(uPpeakIDs)){
    ##original SNP
    PSNPs=read.table(paste0(Pdir,"chr",unlist(strsplit(uPpeakIDs[j],"_"))[1],"/","chr",uPpeakIDs[j],".tped"),header=FALSE);
    PSNPsGR=GRanges(paste0("chr",PSNPs$V1),IRanges(sapply(PSNPs$V2, function(x) as.numeric(unlist(strsplit(x,":",fixed=T))[2])),sapply(PSNPs$V2, function(x) as.numeric(unlist(strsplit(x,":",fixed=T))[2]))));
    PSNPsGR$SNP_ID=PSNPs$V2;
    ##Neuron donor IDs
    PIDfile=read.table(paste0(Pdir,"chr",unlist(strsplit(uPpeakIDs[j],"_"))[1],"/","chr",uPpeakIDs[j],".tfam"),header=FALSE);
    PIDs=paste(PIDfile$V1,PIDfile$V2,sep="_");
    ## use one peaks each loop
    thisoutput=output[which(PpeakIDs==uPpeakIDs[j])];
    ## genotype
    geno=paste0(Pdir,"chr",unlist(strsplit(uPpeakIDs[j],"_"))[1],"/","chr",uPpeakIDs[j]);
    ## phenotype
    pheno=paste0("/proj/steinlab/projects/R00/atac-qtl/EMMAXResult/Phonetype_ATAC/CSAW/Progenitor/chr",uPpeakIDs[j],"_Progenitor.txt");
    ## kinship matrix
    kinship=paste0("/proj/steinlab/projects/R00/atac-qtl/EMMAXResult/KinshipMatrix/Progenitor.",unique(thisoutput$chrnames[1]),".BN.kinf");
    ## genotype on this chromosome
    traw=read.table(paste0("/proj/steinlab/projects/R00/atac-qtl/EMMAXResult/Genotype_Hg38/PlinkPedMapFiles/",unique(thisoutput$chrnames[1]),".dose.R2g03.QC.traw"),header=TRUE);
    ##find GWAS index SNPs
    ##overlapped SNPs
    olap=findOverlaps(PSNPsGR,hg38resultGR);
    ohg38resultGR=hg38resultGR[subjectHits(olap)];
    ohg38resultGR$SNP_ID=PSNPsGR$SNP_ID[queryHits(olap)];
    ##calculate LD r2,only keep the SNPs have rsid
    thisoutput=thisoutput[grep("rs",thisoutput$MarkerName)];
    for (i in 1:length(thisoutput)){
        ##find index gSNP 
        testSNP=thisoutput$MarkerName[i];
        system(paste0("module add plink/1.90b3;plink --bfile /proj/steinlab/projects/1000genomes/phase3EURhg38/ALL.chr",thisoutput$CHR_A[1],"_GRCh38.genotypes.20170504.EUR --r2 --ld-window-r2 0.2 --ld-window-kb 10000 --ld-window 2000 --ld-snp ",testSNP));
        LD=read.table("plink.ld",header=T);
        SNP_BGR=GRanges(paste0("chr",c(unique(LD$CHR_A),LD$CHR_B)),IRanges(c(unique(LD$BP_A),LD$BP_B),c(unique(LD$BP_A),LD$BP_B)));
        thisohg38resultGR=ohg38resultGR[unique(queryHits(findOverlaps(ohg38resultGR,SNP_BGR)))];
        if (length(thisohg38resultGR)>0){
           ## SNP ID with the min Pvalue
           thisSNP=thisohg38resultGR$SNP_ID[which(thisohg38resultGR$P < 5e-08)];
           thisrsid=thisohg38resultGR$MarkerName[which(thisohg38resultGR$P < 5e-08)];
           for (m in 1:length(thisrsid)){
               thisoutput$condSNP=thisrsid[m];
               ## genotype for this SNP
               thistraw=data.frame(geno=traw[which(traw$SNP==thisSNP[m]),match(PIDs,colnames(traw))]);
               ## add genotype to co-variants file 
               thisvars=cbind(vars,t(thistraw));
               write.table(thisvars,file="thisvars.txt",quote=F,row.names=F,col.names=F);
               ## run emmax
               commandline=paste0("module add emmax;emmax -v -d 10 -t ",geno," -p ",pheno, " -k ",kinship," -c thisvars.txt -o chr",uPpeakIDs[j],"_",thisrsid[m],"_Progenitor");
               system(commandline);
           }
        }
    }
    plotdatr=c(plotdatr,thisoutput);
}
save(plotdatr,file="Plotdata_Progenitor.Rdata");


