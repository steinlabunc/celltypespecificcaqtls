library(GenomicRanges);
library(GenomicFeatures);
library(rtracklayer);
library(GenomicAlignments);
options(stringsAsFactors=FALSE);
##read GWAS data
GWASdata=read.table(gzfile("/proj/steinlab/projects/R00/ATACPartitionedHeritability/diseasetraitmunged/origdownloads/BD/daner_PGC_BIP32b_mds7a_0416a.gz"),header=T);
##keep significant SNPs
GWASdata=GWASdata[which(GWASdata$P < 5e-08),];
## make data a GRanges
resultGR = GRanges(paste0("chr",GWASdata$CHR),IRanges(GWASdata$BP,GWASdata$BP));
mcols(resultGR)=GWASdata;
resultGR=unique(resultGR);
## convert hg19 to hg38
path="/proj/steinlab/projects/R00/atac-qtl/EMMAXResult/DiseaseColocalization/CoordinatesColocalization/ENIGMA3Traits/hg19ToHg38.over.chain";
ch = import.chain(path);
hg38resultGR=unlist(liftOver(resultGR,ch));
##seqnames
chrs=intersect(unique(GWASdata$CHR),1:22);
##save results
output=GRanges();
for (i in 1:length(chrs)){
    ##read in NcaSNPs within peaks and with r2>0.8
    NcaSNPs=read.csv(paste0("/proj/steinlab/projects/R00/atac-qtl/EMMAXResult/DiseaseColocalization/ConditinalCaQTLs/TestCaSNPs/Neuron/SNPs_LD_thresdold_chr",chrs[i],".csv"));
    colnames(NcaSNPs)[8]="chrnames";
    NcaSNPs_A=GRanges(paste0("chr",NcaSNPs$CHR_A),IRanges(NcaSNPs$BP_A,NcaSNPs$BP_A));
    olaps=findOverlaps(NcaSNPs_A, hg38resultGR);
    olapA=hg38resultGR[subjectHits(olaps)];
    mcols(olapA)=cbind(mcols(olapA),NcaSNPs[queryHits(olaps),]);
    
    NcaSNPs_B=GRanges(paste0("chr",NcaSNPs$CHR_B),IRanges(NcaSNPs$BP_B,NcaSNPs$BP_B));
    olaps=findOverlaps(NcaSNPs_B, hg38resultGR);
    olapB=hg38resultGR[subjectHits(olaps)];
    mcols(olapB)=cbind(mcols(olapB),NcaSNPs[queryHits(olaps),]);
     
    olap=unique(c(olapA,olapB));
    if (length(olap)>0){
       cat("Got ",length(olap)," SNPs overlapped on chr",i,"...\n");
       output=c(output,olap);
    }
}
if (length(output)>0){
   save(output,file="OverlappedSNPs_Neuron.Rdata");
}




