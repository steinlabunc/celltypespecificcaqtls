#!/bin/bash

#SBATCH -p general
#SBATCH -n 1
#SBATCH --mem=10g
#SBATCH -t 40:00:00

module add plink/1.90b3;


dir="/proj/steinlab/projects/R00/atac-qtl/EMMAXResult/Neuron/CSAW/PCAs_7/Neuron/RawData";

#mkdir /proj/steinlab/projects/R00/atac-qtl/EMMAXResult/Neuron/CSAW/PCAs_7/Neuron/ClumpedData/chr$1;

files=`ls $dir/chr$1/*.ps`;

for file in $files
do
filename=$(basename $file);

#sed -i '1i SNP BETA P' $file;

plink --bfile /pine/scr/d/a/danliang/EMMAXResult/CSAW/Genotype_hg38/FullChrFiles/$1.dose.R2g03.QC.IDfixed --keep /proj/steinlab/projects/R00/atac-qtl/EMMAXResult/Genotype_Hg38/TruncatedFiles/KeepIds_Neuron.txt --clump $file --clump-kb 250 --clump-p1 0.000015 --clump-p2 0.01 --clump-r2 0.50 --allow-no-sex --out ./chr$1/$filename;

done

exit 0;

