options(stringsAsFactors=FALSE);


Nbetapval=data.frame();
Pbetapval=data.frame();
for (j in c(1:22,"X")){
cat("chr",j,"...\n");
NPeakSNPRawfile=paste0("/proj/steinlab/projects/R00/atac-qtl/EMMAXResult/Neuron/CSAW/PCAs_7/Neuron/RawData/chr",j,"/");
NPeakSNPfile="/pine/scr/d/a/danliang/EMMAXResult/CSAW/clumped/Neuron/ClumpedData/";
Npsfiles=list.files(NPeakSNPfile, pattern=glob2rx(paste0("chr",j,"_*.clumped")));
##MAF files
thischrN=read.table(paste0("/proj/steinlab/projects/R00/atac-qtl/EMMAXResult/Genotype_Hg38/",j,".dose.R2g03.QC.Neuron.frqx"),skip=1);
colnames(thischrN)=c("CHR","SNP","A1","A2","HOM_A1","HET","HOM_A2","HAP_A1","HAP_A2","MISSING");
thischrN=thischrN[which(thischrN$HOM_A1>=2 | thischrN$HET>=2),];
if (length(Npsfiles)>0){
for (i in 1:length(Npsfiles)){
    cat("Neuron",i,"...\n");
    thisname=unlist(strsplit(Npsfiles[i],".",fixed=T))[1];
    peakcenter=mean(c(as.numeric(unlist(strsplit(thisname,"_",fixed=T))[2]),as.numeric(unlist(strsplit(thisname,"_",fixed=T))[3])));
    Nps=read.table(paste0(NPeakSNPfile,Npsfiles[i]),header=T);
    RawNps=read.table(paste0(NPeakSNPRawfile,unlist(strsplit(Npsfiles[i],".",fixed=T))[1],"_Neuron.ps"),header=T);
    Nps=Nps[which(Nps$SNP %in% thischrN$SNP),];
    
    NSNPs=c(Nps$SNP,unlist(strsplit(Nps$SP2,",",fixed=T)));
    NSNPs=sapply(NSNPs,function(x) unlist(strsplit(x,"(",fixed=T))[1]);
    NSNPs=thischrN$SNP[which(thischrN$SNP %in% NSNPs)];
    RawNps=RawNps[which(RawNps$SNP %in% NSNPs),];
    RawNps$BP=sapply(RawNps$SNP, function(x) as.numeric(unlist(strsplit(x,":",fixed=TRUE))[2]));
    RawNps=RawNps[which(RawNps$P %in% RawNps$P[match(Nps$SNP,RawNps$SNP)]),];
    if(dim(RawNps)[1]>0){
    Nbetapval=rbind(Nbetapval,data.frame(SNP=RawNps$SNP,Distance=(RawNps$BP-peakcenter),Beta=RawNps$BETA,Pval=-log10(as.numeric(RawNps$P)),seqnames=j,peakstart=as.numeric(unlist(strsplit(thisname,"_",fixed=T))[2]),peakend=as.numeric(unlist(strsplit(thisname,"_",fixed=T))[3]),InPeak=(as.numeric(unlist(strsplit(thisname,"_",fixed=T))[2]) <= RawNps$BP & as.numeric(unlist(strsplit(thisname,"_",fixed=T))[3])>=RawNps$BP)));
    
}}}
#if(length(Ppsfiles)>0){
#for (i in 1:length(Ppsfiles)){    
#    cat("Progenitor",i,"...\n");
#    peakcenter=mean(c(as.numeric(unlist(strsplit(Ppsfiles[i],"_",fixed=T))[2]),as.numeric(unlist(strsplit(Ppsfiles[i],"_",fixed=T))[3])));
#    Pps=read.table(paste0(PPeakSNPfile,Ppsfiles[i]),header=T);
#    RawPps=read.table(paste0(PPeakSNPRawfile,unlist(strsplit(Ppsfiles[i],".",fixed=T))[1],".ps"),header=T);
#    Pps=Pps[which(Pps$SNP %in% thischrP$SNP),];
#    PSNPs=c(Pps$SNP,unlist(strsplit(Pps$SP2,",",fixed=T)));
#    PSNPs=sapply(PSNPs,function(x) unlist(strsplit(x,"(",fixed=T))[1]);
#    PSNPs=thischrP$SNP[which(thischrP$SNP %in% PSNPs)];
#    RawPps=RawPps[which(RawPps$SNP %in% PSNPs),];
#    RawPps$BP=sapply(RawPps$SNP, function(x) as.numeric(unlist(strsplit(x,":",fixed=TRUE))[2]));
#    RawPps=RawPps[which(RawPps$P %in% RawPps$P[match(Pps$SNP,RawPps$SNP)]),];
#    if(dim(RawPps)[1]>0){
    
#    Pbetapval=rbind(Pbetapval,data.frame(SNP=RawPps$SNP,Distance=(RawPps$BP-peakcenter),Beta=RawPps$BETA,Pval=-log10(RawPps$P),InPeak=(as.numeric(unlist(strsplit(Ppsfiles[i],"_",fixed=T))[2]) <= RawPps$BP & as.numeric(unlist(strsplit(Ppsfiles[i],"_",fixed=T))[3])>=RawPps$BP)));  
#}}
#    #save(Nbetamatrix,Npvalmatrix,Pbetamatrix,Ppvalmatrix,file="NPbetaPMatrix.Rdata");
#}

#save(Nbetapval,Pbetapval,file="NPbetaPMatrix.Rdata");

#load(paste0("NPbetaPMatrix_chr",j,".Rdata"));
#totalNbetapval=rbind(totalNbetapval,Nbetapval);  
#totalPbetapval=rbind(totalPbetapval,Pbetapval);

#}    
}
#Nbetapval=totalNbetapval;
#Pbetapval=totalPbetapval;
save(Nbetapval,file="NPbetaP.Rdata");
Nbetapval = Nbetapval[which(Nbetapval$Distance > -100000 & Nbetapval$Distance < 100000),];
#Pbetapval = Pbetapval[which(Pbetapval$Distance > -100000 & Pbetapval$Distance < 100000),];

cat("Neuron caQTLs FDR < 0.05 ",length(which(Nbetapval$Pval >= -log10(1.491204e-05))),"...\n");
#cat("Progenitor caQTLs FDR < 0.05 ",length(which(Pbetapval$Pval >= -log10(2.823784e-05))),"...\n");


pdf("NPbetaPMatrix_clumped_PCAs_7.pdf");

Nbetapval=Nbetapval[which(Nbetapval$Pval>= -log10(1.491204e-05)),];

#Pbetapval=Pbetapval[which(Pbetapval$Pval>= -log10(2.823784e-05)),];

write.csv(Nbetapval,"Neuron_caQTLs_clumped_PC1_7.csv",quote=F,row.names=F);

if(dim(Nbetapval)[1]>0){
plot(Nbetapval$Distance,Nbetapval$Beta,pch=1,col="black",main="Neuron Beta",xlab="Distance of genetic variants to peak center",ylab="abs(Beta)");
points(Nbetapval$Distance[which(Nbetapval$InPeak=="TRUE")],Nbetapval$Beta[which(Nbetapval$InPeak=="TRUE")],col="red");
plot(Nbetapval$Distance,Nbetapval$Pval,pch=1,col="black", main="Neuron P-value",xlab="Distance of genetic variants to peak center",ylab="-log10(P-value)");
points(Nbetapval$Distance[which(Nbetapval$InPeak=="TRUE")],Nbetapval$Pval[which(Nbetapval$InPeak=="TRUE")],col="red");

qts <- quantile(Nbetapval$Distance,probs=c(.05,.95));
plot(density(Nbetapval$Distance),main="Neuron caQTLs Vs. Distance", ylab="caQTL Density");
abline(v=qts[1],col="red");
abline(v=qts[2],col="red");
cat("Neuron caQTLs distance interval [",qts[1],qts[2],"]...\n");
hist(Nbetapval$Distance,main="Neuron caQTLs Vs. Distance",ylab="caQTL Number");
}
#if(dim(Pbetapval)[1]>0){
#plot(Pbetapval$Distance,Pbetapval$Beta,pch=1,col="black",main="Progenitor Beta",xlab="Distance of genetic variants to peak center",ylab="abs(Beta)");
#points(Pbetapval$Distance[which(Pbetapval$InPeak=="TRUE")],Pbetapval$Beta[which(Pbetapval$InPeak=="TRUE")],col="red");
#plot(Pbetapval$Distance,Pbetapval$Pval,pch=1,col="black", main="Progenitor P-value",xlab="Distance of genetic variants to peak center",ylab="-log10(P-value)");
#points(Pbetapval$Distance[which(Pbetapval$InPeak=="TRUE")],Pbetapval$Pval[which(Pbetapval$InPeak=="TRUE")],col="red");

#qts <- quantile(Pbetapval$Distance,probs=c(.05,.95));
#plot(density(Pbetapval$Distance),main="Progenitor caQTLs Vs. Distance", ylab="caQTL Density");
#abline(v=qts[1],col="red");
#abline(v=qts[2],col="red");
#cat("Progenitor caQTLs distance interval [",qts[1],qts[2],"]...\n");
#hist(Pbetapval$Distance,main="Progenitor caQTLs Vs. Distance",ylab="caQTL Number");
#}

dev.off();










