options(stringsAsFactors=FALSE);
## TFs esults
results=read.csv("MotifbreakRResultsTable.csv");
Nresults=results[which(results$caQTLCellType=="Neuron"),];
Presults=results[which(results$caQTLCellType=="Progenitor"),];
N_TFs=unique(Nresults$geneSymbol);
P_TFs=unique(Presults$geneSymbol);
NcaQTLs=read.csv("/proj/steinlab/projects/R00/atac-qtl/EMMAXResult/SNPAffectedTFBS/Neuron_all_caQTLs_PC1_7_wA1A2_wSNP.csv");
index=which(nchar(NcaQTLs$A1)!=1 | nchar(NcaQTLs$A2)!=1);
NcaQTLs=NcaQTLs[-index,];
PcaQTLs=read.csv("/proj/steinlab/projects/R00/atac-qtl/EMMAXResult/SNPAffectedTFBS/Progenitor_all_caQTLs_PC1_4_wA1A2_wSNP.csv");
index=which(nchar(PcaQTLs$A1)!=1 | nchar(PcaQTLs$A2)!=1);
PcaQTLs=PcaQTLs[-index,];
Nslopes=data.frame();
Pslopes=data.frame();
pdf("VariantEffectpattern.pdf");
for (i in 1:length(N_TFs)){
    thisNresults=Nresults[which(Nresults$geneSymbol==N_TFs[i]),];
    thisNcaQTLs=NcaQTLs[which(NcaQTLs$rsid %in% thisNresults$rsid),];    
    thisNresults$A1=thisNcaQTLs$A1[match(thisNresults$rsid,thisNcaQTLs$rsid)];
    thisNresults$A2=thisNcaQTLs$A2[match(thisNresults$rsid,thisNcaQTLs$rsid)];
    thisNresults$Beta=thisNcaQTLs$Beta[match(thisNresults$rsid,thisNcaQTLs$rsid)];
    thisNresults$DeltaScore=thisNresults$scoreRef-thisNresults$scoreAlt;
    Nsigns=rep(1,dim(thisNresults)[1]);
    thisplot=thisNresults[,c(1,6:7,17:18,25:28)];
    Nsigns[which(as.character(thisplot$A2)!=as.character(thisplot$ALT))]=-1;
    thisplot$DeltaScore=Nsigns*thisplot$DeltaScore;
    thisplot=unique(thisplot);
    ## change signs for plot data
    posindex=which(thisplot$Beta * thisplot$DeltaScore > 0);
    negindex=which(thisplot$Beta * thisplot$DeltaScore < 0);
    thisplot$Beta[posindex]=abs(thisplot$Beta[posindex]);
    thisplot$DeltaScore[posindex]=abs(thisplot$DeltaScore[posindex]);
    thisplot$Beta[negindex]= -abs(thisplot$Beta[negindex]);
    thisplot$DeltaScore[negindex]=abs(thisplot$DeltaScore[negindex]);

    if (length(Nsigns)==1){
       slope=as.numeric(thisplot$DeltaScore)/as.numeric(thisplot$Beta);
       pvalue=NA;
       Nslopes=rbind(Nslopes,data.frame(TFs=N_TFs[i],Slope=slope,Pvalue=pvalue,N=length(Nsigns)));
    }else{
       Nfit=lm(thisplot$Beta~0+thisplot$DeltaScore);
       slope=summary(Nfit)$coefficients[1,1];
       pvalue=summary(Nfit)$coefficients[1,4];     
       Nslopes=rbind(Nslopes,data.frame(TFs=N_TFs[i],Slope=slope,Pvalue=pvalue,N=length(Nsigns)));
       plot(thisplot$Beta,thisplot$DeltaScore,ylim=c(0,max(thisplot$DeltaScore)),xlim=c(-max(abs(thisplot$Beta)),max(abs(thisplot$Beta))),main=paste0("Neuron ",N_TFs[i]),xlab="Beta",ylab="Relative Entropy (Non-effect_allele - Effect_allele)");
       abline(a=0,b=1/slope,col="red");
    }
}
write.csv(Nslopes,file="Neuron_variants_affect_slopes.csv",quote=F,row.names=F);
for (i in 1:length(P_TFs)){
    thisPresults=Presults[which(Presults$geneSymbol==P_TFs[i]),];
    thisPcaQTLs=PcaQTLs[which(PcaQTLs$rsid %in% thisPresults$rsid),];   
    thisPresults$A1=thisPcaQTLs$A1[match(thisPresults$rsid,thisPcaQTLs$rsid)];
    thisPresults$A2=thisPcaQTLs$A2[match(thisPresults$rsid,thisPcaQTLs$rsid)];
    thisPresults$Beta=thisPcaQTLs$Beta[match(thisPresults$rsid,thisPcaQTLs$rsid)];
    thisPresults$DeltaScore=thisPresults$scoreRef-thisPresults$scoreAlt;    
    thisplot=thisPresults[,c(1,6:7,17:18,25:28)];
    Psigns=rep(1,dim(thisplot)[1]);
    Psigns[which(as.character(thisplot$A2)!=as.character(thisplot$ALT))]=-1;
    thisplot$DeltaScore=Psigns*thisplot$DeltaScore;
    thisplot=unique(thisplot);
    ## change signs for plot data
    posindex=which(thisplot$Beta * thisplot$DeltaScore > 0);
    negindex=which(thisplot$Beta * thisplot$DeltaScore < 0);
    thisplot$Beta[posindex]=abs(thisplot$Beta[posindex]);
    thisplot$DeltaScore[posindex]=abs(thisplot$DeltaScore[posindex]);
    thisplot$Beta[negindex]= -abs(thisplot$Beta[negindex]);
    thisplot$DeltaScore[negindex]=abs(thisplot$DeltaScore[negindex]);
    if (length(Psigns)==1){
       slope=as.numeric(thisplot$DeltaScore)/as.numeric(thisplot$Beta);
       pvalue=NA;
       Pslopes=rbind(Pslopes,data.frame(TFs=P_TFs[i],Slope=slope,Pvalue=pvalue,N=length(Psigns)));
    }else{
       Pfit=lm(thisplot$Beta~0+thisplot$DeltaScore);
       slope=summary(Pfit)$coefficients[1,1];
       pvalue=summary(Pfit)$coefficients[1,4];
       Pslopes=rbind(Pslopes,data.frame(TFs=P_TFs[i],Slope=slope,Pvalue=pvalue,N=length(Psigns)));
       plot(thisplot$Beta,thisplot$DeltaScore,ylim=c(0,max(thisplot$DeltaScore)),xlim=c(-max(abs(thisplot$Beta)),max(abs(thisplot$Beta))),main=paste0("Progenitor ",P_TFs[i]),xlab="Beta",ylab="Relative Entropy (Non-effect_allele - Effect_allele)");
       abline(a=0,b=1/slope,col="red");
    }
}
write.csv(Pslopes,file="Progenitor_variants_affect_slopes.csv",quote=F,row.names=F);
dev.off();
##filter and calculate FDR
fPslopes=Pslopes[which(Pslopes$N>=10),];
fNslopes=Nslopes[which(Nslopes$N>=10),];
fPslopes$FDR=p.adjust(fPslopes$Pvalue,method="fdr");
fNslopes$FDR=p.adjust(fNslopes$Pvalue,method="fdr");
sigfPslopes=fPslopes[which(fPslopes$FDR<0.05),];
sigfNslopes=fNslopes[which(fNslopes$FDR<0.05),];

write.csv(sigfNslopes,file="Neuron_variants_affect_slopes_sig.csv");  
write.csv(sigfPslopes,file="Progenitor_variants_affect_slopes_sig.csv");


##separate for different TF families
##using JASPAR2018
library(TFBSTools);
library(JASPAR2018);
opts = list();
opts[["collection"]] = "CORE";
opts[["all_versions"]] = TRUE;
opts[["tax_group"]] = "vertebrates";
opts[["matrixtype"]] = "PWM";
JASPAR2018 = file.path(system.file("extdata", package="JASPAR2018"),"JASPAR2018.sqlite");
pwmatrices = getMatrixSet(JASPAR2018, opts);
TFID=ID(pwmatrices);
TF.classes=matrixClass(pwmatrices);
TFs.names = name(pwmatrices);
TFs.IDs = ID(pwmatrices);
TFnamefamily=data.frame(TFs=TFs.names);
TFclassdata=t(as.data.frame(TF.classes));
TFnamefamily=cbind(TFnamefamily,TFclassdata);
colnames(TFnamefamily)=c("TFs","Family","Family2");
TFnamefamily=unique(TFnamefamily[,1:2]);
unique.TF.classes=unique(TFnamefamily$Family);
NFamilyslopes=data.frame();
PFamilyslopes=data.frame();
pdf("VariantEffectpattern_TFFamily.pdf");
for (i in 1:length(unique.TF.classes)){
    ##Neuron
    thisNresults=Nresults[which(toupper(Nresults$geneSymbol) %in% toupper(TFnamefamily$TFs[which(TFnamefamily$Family==unique.TF.classes[i])])),];
    thisNcaQTLs=NcaQTLs[which(NcaQTLs$rsid %in% thisNresults$rsid),];
    thisNcaQTLs$REF=thisNresults$REF[match(thisNcaQTLs$rsid,thisNresults$rsid)];
    thisNcaQTLs$ALT=thisNresults$ALT[match(thisNcaQTLs$rsid,thisNresults$rsid)];
    thisNcaQTLs$scoreRef=thisNresults$scoreRef[match(thisNcaQTLs$rsid,thisNresults$rsid)];
    thisNcaQTLs$scoreAlt=thisNresults$scoreAlt[match(thisNcaQTLs$rsid,thisNresults$rsid)];
    thisNcaQTLs$DeltaScore=thisNcaQTLs$scoreRef-thisNcaQTLs$scoreAlt;
    Nsigns=rep(1,dim(thisNcaQTLs)[1]);
    thisplot=thisNcaQTLs[,c(3,9,15:17,19:23)];
    Nsigns[which(as.character(thisplot$A2)!=as.character(thisplot$ALT))]=-1;
    thisplot$DeltaScore=Nsigns*thisplot$DeltaScore;
    thisplot=unique(thisplot);
    ## change signs for plot data
    posindex=which(thisplot$Beta * thisplot$DeltaScore > 0);
    negindex=which(thisplot$Beta * thisplot$DeltaScore < 0);
    thisplot$Beta[posindex]=abs(thisplot$Beta[posindex]);
    thisplot$DeltaScore[posindex]=abs(thisplot$DeltaScore[posindex]);
    thisplot$Beta[negindex]= -abs(thisplot$Beta[negindex]);
    thisplot$DeltaScore[negindex]=abs(thisplot$DeltaScore[negindex]);
    if (length(Nsigns) >= 5){
       Nfit=lm(thisplot$Beta~0+thisplot$DeltaScore);
       slope=summary(Nfit)$coefficients[1,1];
       pvalue=summary(Nfit)$coefficients[1,4];
       NFamilyslopes=rbind(NFamilyslopes,data.frame(TFs=unique.TF.classes[i],Slope=slope,Pvalue=pvalue));
       plot(thisplot$Beta,thisplot$DeltaScore,ylim=c(0,max(thisplot$DeltaScore)),xlim=c(-max(abs(thisplot$Beta)),max(abs(thisplot$Beta))),main=paste0("Neuron ",unique.TF.classes[i]),xlab="Beta",ylab="Relative Entropy (Non-effect_allele - Effect_allele)");
       abline(a=0,b=1/slope,col="red");
    }
    ##progenitor
    thisPresults=Presults[which(toupper(Presults$geneSymbol) %in% toupper(TFnamefamily$TFs[which(TFnamefamily$Family==unique.TF.classes[i])])),];
    thisPcaQTLs=PcaQTLs[which(PcaQTLs$rsid %in% thisPresults$rsid),];
    thisPcaQTLs$REF=thisPresults$REF[match(thisPcaQTLs$rsid,thisPresults$rsid)];
    thisPcaQTLs$ALT=thisPresults$ALT[match(thisPcaQTLs$rsid,thisPresults$rsid)];
    thisPcaQTLs$scoreRef=thisPresults$scoreRef[match(thisPcaQTLs$rsid,thisPresults$rsid)];
    thisPcaQTLs$scoreAlt=thisPresults$scoreAlt[match(thisPcaQTLs$rsid,thisPresults$rsid)];
    thisPcaQTLs$DeltaScore=thisPcaQTLs$scoreRef-thisPcaQTLs$scoreAlt;
    Psigns=rep(1,dim(thisPcaQTLs)[1]);
    thisplot=thisPcaQTLs[,c(3,9,15:17,19:23)];
    Psigns[which(as.character(thisplot$A2)!=as.character(thisplot$ALT))]=-1;
    thisplot$DeltaScore=Psigns*thisplot$DeltaScore;
    thisplot=unique(thisplot);
    ## change signs for plot data
    posindex=which(thisplot$Beta * thisplot$DeltaScore > 0);
    negindex=which(thisplot$Beta * thisplot$DeltaScore < 0);
    thisplot$Beta[posindex]=abs(thisplot$Beta[posindex]);
    thisplot$DeltaScore[posindex]=abs(thisplot$DeltaScore[posindex]);
    thisplot$Beta[negindex]= -abs(thisplot$Beta[negindex]);
    thisplot$DeltaScore[negindex]=abs(thisplot$DeltaScore[negindex]);
    if (length(Psigns) >= 5){
       Pfit=lm(thisplot$Beta~0+thisplot$DeltaScore);
       slope=summary(Pfit)$coefficients[1,1];
       pvalue=summary(Pfit)$coefficients[1,4];
       PFamilyslopes=rbind(PFamilyslopes,data.frame(TFs=unique.TF.classes[i],Slope=slope,Pvalue=pvalue));
       plot(thisplot$Beta,thisplot$DeltaScore,ylim=c(0,max(thisplot$DeltaScore)),xlim=c(-max(abs(thisplot$Beta)),max(abs(thisplot$Beta))),main=paste0("Progenitor ",unique.TF.classes[i]),xlab="Beta",ylab="Relative Entropy (Non-effect_allele - Effect_allele)");
       abline(a=0,b=1/slope,col="red");
    }
}
PFamilyslopes$FDR=p.adjust(PFamilyslopes$Pvalue);
NFamilyslopes$FDR=p.adjust(NFamilyslopes$Pvalue);
##save data
write.csv(NFamilyslopes,file="Neuron_variants_affect_slopes_TFfamily.csv",quote=F,row.names=F);
write.csv(PFamilyslopes,file="Progenitor_variants_affect_slopes_TFfamily.csv",quote=F,row.names=F);
dev.off();

















