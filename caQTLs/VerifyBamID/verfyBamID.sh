#!/bin/bash
#SBATCH -n 1
#SBATCH -t 24:00:00
#SBATCH --mem 30720



## verfy bam file ID with genotype
#/proj/steinlab/projects/sharedApps/VerifyBamID/verifyBamID_1.1.3/verifyBamID/bin/verifyBamID --vcf /proj/steinlab/projects/R00/atac-qtl/GenotypeData/PlinkData_original/R00MergedData/R00mgergedGenoMafHweQCVCFhg38.vcf --bam /proj/steinlab/projects/R00/ATACpreprocess/RemvBlacklist/ATAC_CC20_NYGC/R00AL169_adaptertrimed_sort_unique_noMT_noblacklist.bam --out R00AL169VerfyBamID --best;

/proj/steinlab/projects/sharedApps/VerifyBamID/verifyBamID_1.1.3/verifyBamID/bin/verifyBamID --vcf /proj/steinlab/projects/R00/atac-qtl/GenotypeData/PlinkData_original/QCMergedData/allmgergedGenoMafHweQCnewVCFhg38.vcf --bam $1 --out $2 --best;
